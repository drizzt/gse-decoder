#!/usr/bin/env lua

loaded, bit = pcall(require, 'bit')
if not loaded then
    bit = require('libs/bit')
end
require('libs/LibCompress')
require('libs/AceSerializer')

-- Ripped from GSE/API/Serialisation.lua
local B64tobyte = {
    a = 0,
    b = 1,
    c = 2,
    d = 3,
    e = 4,
    f = 5,
    g = 6,
    h = 7,
    i = 8,
    j = 9,
    k = 10,
    l = 11,
    m = 12,
    n = 13,
    o = 14,
    p = 15,
    q = 16,
    r = 17,
    s = 18,
    t = 19,
    u = 20,
    v = 21,
    w = 22,
    x = 23,
    y = 24,
    z = 25,
    A = 26,
    B = 27,
    C = 28,
    D = 29,
    E = 30,
    F = 31,
    G = 32,
    H = 33,
    I = 34,
    J = 35,
    K = 36,
    L = 37,
    M = 38,
    N = 39,
    O = 40,
    P = 41,
    Q = 42,
    R = 43,
    S = 44,
    T = 45,
    U = 46,
    V = 47,
    W = 48,
    X = 49,
    Y = 50,
    Z = 51,
    ["0"] = 52,
    ["1"] = 53,
    ["2"] = 54,
    ["3"] = 55,
    ["4"] = 56,
    ["5"] = 57,
    ["6"] = 58,
    ["7"] = 59,
    ["8"] = 60,
    ["9"] = 61,
    ["("] = 62,
    [")"] = 63
}

-- This code is based on the Encode7Bit algorithm from LibCompress
-- Credit goes to Galmok of European Stormrage (Horde), galmok@gmail.com
-- This version was lifted straight from WeakAuras 2
local decodeB64Table = {}

function decodeB64(str)
    local bit8 = decodeB64Table;
    local decoded_size = 0;
    local ch;
    local i = 1;
    local bitfield_len = 0;
    local bitfield = 0;
    local l = #str;
    while true do
        if bitfield_len >= 8 then
            decoded_size = decoded_size + 1;
            bit8[decoded_size] = string.char(bit.band(bitfield, 255));
            bitfield = bit.rshift(bitfield, 8);
            bitfield_len = bitfield_len - 8;
        end
        ch = B64tobyte[str:sub(i, i)];
        bitfield = bitfield + bit.lshift(ch or 0, bitfield_len);
        bitfield_len = bitfield_len + 6;
        if i > l then
            break
        end
        i = i + 1;
    end
    return table.concat(bit8, "", 1, decoded_size)
end

local compressed = decodeB64(arg[1])
local decompressed = LibCompress:Decompress(compressed)
local _, deserialized = AceSerializer:Deserialize(decompressed)

-- print(json.encode(deserialized))

local entry = deserialized[2]
local macroversions = entry["MacroVersions"]

print("Talents: " .. entry["Talents"])

for i, v in ipairs(macroversions) do
    print("\nVersion: " .. i)
    print("\nStep Function: " .. v["StepFunction"])
    print("Inner Loop Limit: " .. v["LoopLimit"])
    print("\n Key Press:")
    for _, element in ipairs(v["KeyPress"]) do
        print("  " .. element)
    end
    print("\n PreMacro:")
    for _, element in ipairs(v["PreMacro"]) do
        print("  " .. element)
    end
    print("\n Sequence:")
    for _, element in ipairs(v) do
        print("  " .. element)
    end
    print("\n Key Release:")
    for _, element in ipairs(v["KeyRelease"]) do
        print("  " .. element)
    end
    print("\n Post Macro:")
    for _, element in ipairs(v["PostMacro"]) do
        print("  " .. element)
    end
end
