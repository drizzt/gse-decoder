-- Use the new operators available since LUA 5.3
bit = {}
function bit.band(...)
	local ret = -1
	for _, v in ipairs({...}) do
		ret = ret & v
	end
	return ret
end

function bit.bor(...)
	local ret = 0
	for _, v in ipairs({...}) do
		ret = ret | v
	end
	return ret
end

function bit.bxor(...)
	local ret = 0
	for _, v in ipairs({...}) do
		ret = ret ~ v
	end
	return ret
end

function bit.bnot(x)
	return ~(x)
end

function bit.lshift(x, n)
	return (x << n) & 0xffffffff
end

function bit.rshift(x, n)
	return (x >> n) & 0xffffffff
end

return bit
